import 'package:chattie_ui/models/backend.dart';
import 'package:chattie_ui/models/email.dart';

import '../widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../screens/screen.dart';
import '../app_theme.dart';

class ChatPage extends StatelessWidget {
  const ChatPage({
    Key key,
  }) : super(key: key);

   @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView.builder(
            itemCount: Backend().getEmails().length,
            itemBuilder: (context, index) {
              Email email = Backend().getEmails()[index];
              return Dismissible(
                key: ObjectKey(email),
                child: ListTile(
                  subtitle: Text(email.subject),
                  title: Text(email.from),
                  leading: Icon(Icons.brightness_1, color: Colors.purple),
                  trailing: Text(email.dateTime),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailsScreen(email)));
                  },
                  
                ),
                onDismissed: (direction) {
                  Backend().getEmails().remove(index);
                },
                
              );
            }));
}
}