class Email {
  final int id;
  final String from;
  final String subject;
  final String dateTime;
  final String body;
  bool read;

  Email({
     this.id,
     this.from,
    this.subject,
     this.dateTime,
     this.body,
    this.read = false,
});
}